# Pipeline Config
PL_NEW_SCRATCH_ORG=".buildkite/pipelines/new-scratch-org.yml"
PL_NEW_PACKAGE_BUILD=".buildkite/pipelines/new-package-build.yml"
PL_SCRIPTS=".buildkite/scripts/"
PL_UTILS=".buildkite/scripts/buildkite-utils.sh"

# Dev Hub Config
DH_ALIAS="DevHub"
DH_USERNAME="mheath@deloitte.co.uk.package"

# Scratch Org config
SO_ALIAS="ScratchOrg"
SO_NAME="Alpha-DEV-"
SO_EDITION="Developer"
SO_DEF="./config/dev-scratch-def.json"
SO_DURATION=7

USER_SKIP="FALSE"

USER_EMAIL="mheath@deloitte.co.uk"
USER_COMPLEXITY=3
USER_ALIAS="admin-user"
USER_PERMSETS="FinancialServicesCloudBasic FinancialServicesCloudStandard FinancialServicesCloudExtension"
#USER_USERNAME="mheath@deloitte.co.uk.so2"
#USER_DEF="./config/user-def.json"

# Package Config
MANAGED_NAME="alpha-managed-core"
MANAGED_DIR="alpha-managed-core"
MANAGED_KEY="AlphA123"
UNLOCKED_NAME="alpha-unlocked"
UNLOCKED_DIR="alpha-unlocked"
UNLOCKED_KEY="AlphA321"

# Source Config
SOURCES="alpha-managed-core alpha-managed-feature-1"

# Test Config
TEST_LEVEL="RunLocalTests"
SPECIFIED_TESTS=""
TEST_OUTPUT="TestLog.log"

# FSC Packages
# FSC_PACKAGE="04t1E000000jbFt"
# FSC_EXTENSION="04t1E000001Iql5"
PACKAGES="04t1E000000jbFt"

# Authentication Config
CONSUMER_KEY="3MVG9I9urWjeUW07nn8c4ZXF7RxeDqKs1Nxjqnqjcw9XYuj4PS2txCzNwTcueTNQ3cv5N7qvmrCZZNaegIOKA"
KEY_DIR=".buildkite/auth/server.key"

# BK Testing
PL_A=".buildkite/pipelines/pipeline-a.yml"