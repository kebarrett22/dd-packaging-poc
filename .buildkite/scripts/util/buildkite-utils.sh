function getVal() {
    VAL=$(buildkite-agent meta-data get $1)
    echo $VAL
}
function setVal() {
    printf "Setting=$1 and $2"
    buildkite-agent meta-data set $1 $2
}

# Styling
GREEN='\033[90m=== \033[32m'
YELLOW='\033[90m=== \033[33m'
RED='\033[90m=== \033[31m'
CYAN='\033[90m=== \033[36m'
END='\033[0m' # No Color
