function getVal() {
    RET=$(buildkite-agent meta-data get $1)
    echo $RET
}
function setVal() {
    printf "Setting: $1 and $2"
    buildkite-agent meta-data set $1 $2
}