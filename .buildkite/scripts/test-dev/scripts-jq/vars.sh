#!/bin/bash

source scripts/buildkite-util.sh
# DevHub
DH_ALIAS: "DevHub"
    DH_USERNAME: "mheath@deloitte.co.uk.package"
    CONSUMER_KEY: "3MVG9I9urWjeUW07nn8c4ZXF7RxeDqKs1Nxjqnqjcw9XYuj4PS2txCzNwTcueTNQ3cv5N7qvmrCZZNaegIOKA"
    KEY_DIR: "./scripts/server.key"

    # Scratch Org config
    USER_USERNAME: "mheath@deloitte.co.uk.so2"
    USER_EMAIL: "mheath@deloitte.co.uk"
    USER_ALIAS: "admin-user"
    USER_DEF: "./config/user-def.json"
    USER_PERMSETS: ("FinancialServicesCloudBasic" "FinancialServicesCloudStandard" "FinancialServicesCloudExtension")
    USER_PERMSETS_STR: "FinancialServicesCloudBasic,FinancialServicesCloudStandard,FinancialServicesCloudExtension"
    SO_ALIAS: "fscscratch2"
    SO_DEF: "./config/project-scratch-def.json"
    SO_DURATION: 7

    # Internal Package dirs
    MANAGED_NAME: "Alpha Managed"
    UNLOCKED_NAME: "alpha-unlocked"
    MANAGED_DIR: "base-managed"
    UNLOCKED_DIR: "base-unlocked"
    TEST_LEVEL: "RunSpecifiedTests"
    SPECIFIED_TESTS: "NewClassA_Test,NewClassB_Test"
    MANAGED_KEY: "AlphA123"
    UNLOCKED_KEY: "AlphA321"

    # FSC Packages
    FSC_PACKAGE: "04t1E000000jbFt"
    FSC_EXTENSION: "04t1E000001Iql5"

# Styling
GREEN: '\033[90m: : :  \033[32m'
YELLOW: '\033[90m: : :  \033[33m'
RED: '\033[90m: : :  \033[31m'
CYAN: '\033[90m: : :  \033[36m'
END: '\033[0m' # No Color