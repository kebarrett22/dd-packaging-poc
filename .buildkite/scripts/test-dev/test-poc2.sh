#!/bin/bash
#source $PL_UTILS
https://mheath-dd:FxHzfJ5uSPGxV6WFYWWp@bitbucket.org/mheath-dd/dd-packaging-poc.git
git rev-list --all | (
while read revision; do
    git grep -F 'set-upstream origin' $revision
done
)
printf "Promoting Package: "
VER_DESC="Desc"
REL_NOTES="https://dd-mheath.com/release"
POST_URL="https://dd-mheath.com/posturl"
git config user.name buildkite-agent
git config user.email mheath@deloitte.co.uk
git remote set-url origin "https://mheath-dd:FxHzfJ5uSPGxV6WFYWWp@bitbucket.org/mheath-dd/dd-packaging-poc.git"

cat sfdx-project.json
sfdx force:package:version:create --package "alpha-unlocked" --installationkey "Alpha123" --skipvalidation --wait 10
cat sfdx-project.json

jq -S . sfdx-project.json > sfdx-project-sorted.json
LATEST=($(jq '.packageAliases | keys | .[-1:] ' sfdx-project-sorted.json))
LATEST=${LATEST[1]}
LATEST=${LATEST:1:-2}
echo $LATEST
if [[ -z "$LATEST" ]]; then {
    echo "Latest incorrect"
    exit 1
};
fi

git checkout -b "package-$LATEST"
git push --set-upstream origin "package-$LATEST"
git add sfdx-project.json
git commit -m "New package version: $LATEST [skip ci]"
git push
