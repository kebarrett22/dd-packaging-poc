#!/bin/bash
source $PL_UTILS


git config user.name buildkite-agent
git config user.email mheath@deloitte.co.uk
git remote set-url origin "https://mheath-dd:FxHzfJ5uSPGxV6WFYWWp@bitbucket.org/mheath-dd/dd-packaging-poc.git"

# Get Package Version Alias 
jq -S . sfdx-project.json > sfdx-project-sorted.json
LATEST=($(jq '.packageAliases | keys | .[-1:] ' sfdx-project-sorted.json))
LATEST=${LATEST[1]}
LATEST=${LATEST:1:-2}
echo $LATEST
if [[ -z "$LATEST" ]]; then {
    echo "Latest incorrect"
    exit 1
};
fi
PACKAGE_COMMIT_MSG='New package version - \"${LATEST}\":\"$(getVal "package_version_id")\"'

git checkout $1
git add sfdx-project.json
git commit -m $PACKAGE_COMMIT_MSG
git push