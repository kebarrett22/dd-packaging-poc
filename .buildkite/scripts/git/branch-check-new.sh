COMMIT=${BUILDKITE_COMMIT}
BRANCH=${BUILDKITE_BRANCH}
MAIN_BRANCH="main"
git config user.name buildkite-agent
git config user.email mheath@deloitte.co.uk
git remote set-url origin "https://mheath-dd:FxHzfJ5uSPGxV6WFYWWp@bitbucket.org/mheath-dd/dd-packaging-poc.git"


function is_new_branch() {

    MAIN_SHA=$(git rev-parse --short $MAIN_BRANCH)
    BRANCH_SHA=$(git rev-parse --short HEAD)
    echo "BRANCH: $BRANCH"
    echo "COMMIT: $BRANCH_SHA"
    echo "$MAIN_SHA <> $BRANCH_SHA"
    if [[ $MAIN_SHA == $BRANCH_SHA ]]; then { 
        printf "${GREEN} New branch detected ${END}\n"
    } else {
        printf "${GREEN} New commit detected, build exited gracefully ${END}\n"
        exit 255
    }
    fi
}
