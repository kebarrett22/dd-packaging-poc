#!/bin/bash
source $PL_UTILS

git push --set-upstream origin "package-$$BUILDKITE_BRANCH"
git add artifacts/log.txt
git commit -m "[skip ci] updating"
git push