#!/bin/bash
source $PL_UTILS

printf "${YELLOW} Creating Scratch Org: $SO_ALIAS ${END}\n"

# Configuring SO number, no need to increment as DH is included in total
# There is a bug with SOs being counted as nonScratchOrgs so both included
ORGS=$(
    sfdx force:org:list \
    --all \
    --json
)
SO_COUNT=$(jq -n --argjson data "$ORGS" '$data.result.scratchOrgs | length')
NSO_COUNT=$(jq -n --argjson data "$ORGS" '$data.result.nonScratchOrgs | length')
SO_NUMBER=$(($SO_COUNT + $NSO_COUNT))

# --targetdevhubusername used for later when cycling, using alias for now as should be consistent regardless of username
SCRATCH_ORG=$(
    sfdx force:org:create --definitionfile $SO_DEF \
    --setalias $SO_ALIAS \
    --targetdevhubusername $DH_ALIAS \
    --setdefaultusername orgName=$SO_NAME$SO_NUMBER \
    edition=$SO_EDITION \
    adminEmail=$USER_EMAIL \
    --durationdays $SO_DURATION \
    --json
)

if [[ $SCRATCH_ORG == *"\"status\": 0"* ]]; then {
    printf "${GREEN} Scratch Org Created Successfully${END}\n"
    DEF_ORG=$(
        sfdx config:set \
        defaultusername=$SO_ALIAS \
        -g \
        --json
    )
    if [[ $DEF_ORG == *"\"status\": 0"* ]]; then {
        printf "${GREEN} Scratch Org Set as Default Org${END}\n"
    }; else
        {
            printf "${RED} WARNING: Scratch Org not set as default${END}\n"
            printf "${CYAN} MESSAGE: $DEF_ORG ${END}\n"
        }
    fi
}; else
    {
        printf "${RED} Scratch Org Created Not created${END}\n"
        printf "${CYAN} MESSAGE: $SCRATCH_ORG ${END}\n"
        exit 1
    }
fi
