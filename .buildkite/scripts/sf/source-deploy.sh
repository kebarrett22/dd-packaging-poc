#!/bin/bash
source $PL_UTILS

# Push will deploy from all paths in sfdx-project.json so specified directories arg more flexibile



VALIDATE_FAILS=()
#DIRS=("$@")
#for DIR in ${DIRS[@]}; do {
#    printf "${YELLOW} Validating dir $DIR ${END}\n"
#    VALIDATE=$(sfdx force:source:deploy -p $DIR -l $TEST_LEVEL --checkonly --json)
#    if [[ $VALIDATE == *"\"status\": 0"* ]]; then {
#        printf "${GREEN} Source validated: $DIR ${END}\n"
#    }; else
#        {
#            VALIDATE_FAILS+=($DIR)
#            printf "${RED} Failed to validate: $DIR ${END}\n"
#            printf "${CYAN} MESSAGE: $VALIDATE {END}\n"
#        }
#    fi
#};
#done


printf "${YELLOW} Validating dirs $1 ${END}\n"
VALIDATE=$(sfdx force:source:deploy -p $1 -l $TEST_LEVEL --checkonly --json)
if [[ $VALIDATE == *"\"status\": 0"* ]]; then {
    printf "${GREEN} Source validated: $DIR ${END}\n"
}; else
    {
        VALIDATE_FAILS+=($1)
        printf "${RED} Failed to validate: $1 ${END}\n"
        printf "${CYAN} MESSAGE: $VALIDATE {END}\n"
    }
fi

if [[ ${#VALIDATE_FAILS[@]} != 0 ]]; then {
        printf "${RED} Failed to validate ${VALIDATE_FAILS[@]} source dirs ${END}\n"
        exit 1
    }
fi

# Validation should provide Quick Deploy IDs, however they wont if no Apex tests are ran so this is slightly slower but will ensure all are attempted for now
DEPLOY_FAILS=()
#for DIR in ${DIRS[@]}; do {
#    printf "${YELLOW} Deploying from dir $DIR ${END}\n"
#    PUSH=$(sfdx force:source:deploy -p $DIR --json)
#    if [[ $PUSH == *"\"status\": 0"* ]]; then {
#        printf "${GREEN} Source deployed: $DIR ${END}\n"
#    }; else
#        {
#            DEPLOY_FAILS+=($DIR)
#            printf "${RED} Failed to deploy: $DIR ${END}\n"
#            printf "${CYAN} MESSAGE: $PUSH {END}\n"
#        }
#    fi
#};
#done

printf "${YELLOW} Deploying from dirs $1 ${END}\n"
    PUSH=$(sfdx force:source:deploy -p $1 --json)
    if [[ $PUSH == *"\"status\": 0"* ]]; then {
        printf "${GREEN} Source deployed: $1 ${END}\n"
    }; else
        {
            DEPLOY_FAILS+=($1)
            printf "${RED} Failed to deploy: $1 ${END}\n"
            printf "${CYAN} MESSAGE: $PUSH {END}\n"
        }
    fi

if [[ ${#DEPLOY_FAILS[@]} != 0 ]]; then {
        printf "${RED} Failed to deploy ${DEPLOY_FAILS[@]} source dirs ${END}\n"
        exit 1
    }
fi
