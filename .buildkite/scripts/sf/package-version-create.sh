#!/bin/bash
source $PL_UTILS

# params(Package Name, Package Dir, Package Key)
function create_package_version() {
    PACKAGE_VERSION_RESULT=$(sfdx force:package:version:create --package $1 \
    --path $2 \
    --installationkey $3 \
    --targetdevhubusername $DH_ALIAS \
    --codecoverage \
    --wait 10 \
    --json)
    
    if [[ $PACKAGE_VERSION_RESULT == *"\"status\": 0"* ]]; then {
        printf "${GREEN} Package: $1 created successfully ${END}\n"
        PACKAGE_VERSION_ID=$(jq -n --argjson data "$PACKAGE_VERSION_RESULT" '$data.result.SubscriberPackageVersionId')
        printf ("${GREEN} Package Id: $PACKAGE_VERSION_ID ${END}\n"
        setVal "package_version_id" $PACKAGE_VERSION_ID
    } else {
        printf "${RED} Package: $1 failed to create ${END}\n"
        printf "${CYAN} MESSAGE: $PACKAGE_VERSION_RESULT ${END}\n"
        exit 1
    }
    fi
}

PACKAGE_NAME=$(getVal "package_name")
PACKAGE_DIR=$(getVal "package_dir") 
PACKAGE_KEY=$(getVal "package_key")

printf "${YELLOW} Creating Package Version: $PACKAGE_NAME ${END}\n"
create_package_version $PACKAGE_NAME $PACKAGE_DIR $PACKAGE_KEY