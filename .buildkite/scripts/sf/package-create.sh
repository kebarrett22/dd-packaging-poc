#!/bin/bash
source $PL_UTILS

# params(Package Name, Package Type, Package Dir, Package Desc)
function create_package() {
    PACKAGE_RESULT=$(sfdx force:package:create --name $1 \
    --packagetype $2 \
    --path $3 \
    --description $4 \
    --targetdevhubusername $DH_ALIAS \
    --json)
    if [[ $PACKAGE_RESULT == *"\"status\": 0"* ]]; then {
        printf "${GREEN} Package: $1 created successfully ${END}\n"
        PACKAGE_ID=$(jq -n --argjson data "$PACKAGE_RESULT" '$data.result.Id')
        printf "${GREEN} Package Id: $PACKAGE_ID ${END}\n"
        setVal "package_id" $PACKAGE_ID
    } else {
        printf "${RED} Package: $1 failed to create ${END}\n"
        printf "${CYAN} MESSAGE: $PACKAGE_RESULT ${END}\n"
        exit 1
    }
    fi
}

# Check if Package exists
PACKAGE_NAME=""
PACKAGE_DIR=""

if [[ $PACKAGE_TYPE == "Managed" ]]; then {
    PACKAGE_NAME=$MANAGED_NAME
    PACKAGE_DIR=$MANAGED_DIR
    setVal "package_key" $MANAGED_KEY
} elif [[ $PACKAGE_TYPE == "Unlocked" ]]; then {
    PACKAGE_NAME=$UNLOCKED_NAME
    PACKAGE_DIR=$UNLOCKED_DIR
    setVal "package_key" $UNLOCKED_KEY
} else {
    printf "${RED} Invalid Package Name: $PACKAGE_NAME ${END}\n"
    exit 1
}
fi

PACKAGE_LIST=$(sfdx force:package:list \
--targetdevhubusername $DH_ALIAS \
--json)
if [[ $PACKAGE_LIST == *"$PACKAGE_NAME"* ]]; then {
        printf "${YELLOW} Package: $PACKAGE_NAME already exists ${END}\n"
    }
else {
    printf "${YELLOW} Creating Package: $PACKAGE_NAME ${END}\n"
    create_package $PACKAGE_NAME $PACKAGE_TYPE $PACKAGE_DIR $PACKAGE_NAME
}
fi

# Meta-data for versioning
setVal "package_name" $PACKAGE_NAME
setVal "package_dir" $PACKAGE_DIR