#!/bin/bash
source $PL_UTILS

printf "${YELLOW} Assigning permission sets to $USER_USERNAME in $SO_ALIAS ${END}\n"
PS_FAILS=()

INSTALL_SUC=$(getVal "package_install_success")
if [[ $INSTALL_SUC == "false" ]]; then {
    printf "${YELLOW} Assigning permission set licenses and for $USER_USERNAME in $SO_ALIAS ${END}\n"
    PSL_ASS=$(sfdx force:apex:execute --targetusername $SO_ALIAS --apexcodefile $APEX_ASSIGNPSL --json)
    if [[ $PSL_ASS == *"\"status\": 0"* ]]; then {
        printf "${GREEN} $APEX_ASSIGNPSL executed successfully ${END}\n"
    }; else
        {
            printf "${RED} Failed to execute: $APEX_ASSIGNPSL${END}\n"
            printf "${CYAN} MESSAGE: $PSL_ASS {END}\n"
        }
    fi
}
fi

for PS in ${USER_PERMSETS[@]}; do {
    PS_ASS=$(sfdx force:user:permset:assign -u $SO_ALIAS -n $PS --json)
    if [[ $PS_ASS == *"\"status\": 0"* ]]; then {
        if [[ $PS_ASS == *"not found in target org"* ]]; then {
            PS_FAILS+=($PS)
            printf "${RED} Failed to assign: $PS ${END}\n"
            printf "${CYAN} MESSAGE: $PS_ASS {END}\n"
        }; else
            {
                printf "${GREEN} Permission Set assigned: $PS ${END}\n"
            }
        fi
    }; else
        {
            PS_FAILS+=($PS)
            printf "${RED} Failed to assign: $PS ${END}\n"
            printf "${CYAN} MESSAGE: $PS_ASS {END}\n"
        }
    fi
};
done

if [[ ${#PS_FAILS[@]} != 0 ]]; then {
    printf "${RED} Failed to assign ${#PS_FAILS[@]}/${#USER_PERMSETS[@]} permission sets: ${PS_FAILS[@]} ${END}\n"
};
fi


NEW_USER=$(sfdx force:user:password:generate --targetusername $SO_ALIAS --complexity $USER_COMPLEXITY --json)
if [[ $NEW_USER == *"\"status\": 0"* ]]; then {
    printf "${GREEN} User Details: $NEW_USER ${END}\n"
}; else
    {
        printf "${RED} Failed to generate new password ${END}\n"
        printf "${CYAN} MESSAGE: $NEW_USER ${END}\n"
    }
fi