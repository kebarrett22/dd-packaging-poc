#!/bin/bash
source $PL_UTILS

# use --json param to capture errors
ORG_LIST=$(
    sfdx force:org:list \
    --json
)

if [[ $ORG_LIST == *"\"status\": 0"* ]]; then {
    printf "${YELLOW} Orgs are logged in${END}\n"
    if [[ $ORG_LIST == *"$DH_USERNAME"* ]]; then {
        printf "${GREEN} Already connected to: $DH_USERNAME ${END}\n"
    }; else
        {
            AUTH=$(
                sfdx auth:jwt:grant \
                --username $DH_USERNAME \
                --jwtkeyfile $KEY_DIR \
                --clientid $CONSUMER_KEY \
                --setalias $DH_ALIAS \
                --setdefaultdevhubusername \
                --json
            )
            if [[ $AUTH == *"\"status\": 0"* ]]; then {
                printf "${GREEN} Successfully authorized: $DH_USERNAME ${END}\n"
            }; else
                {
                    printf "${RED} ERROR: Failed to authorize: $DH_USERNAME ${END}\n"
                    printf "${CYAN} MESSAGE: $AUTH ${END}\n"
                    exit 1
                }
            fi
        }
    fi
}; else
    {
        printf "${YELLOW} No orgs connected, attempting to authorize: $DH_USERNAME ${END}\n"
        AUTH=$(
            sfdx auth:jwt:grant \
            --username $DH_USERNAME \
            --jwtkeyfile $KEY_DIR \
            --clientid $CONSUMER_KEY \
            --setalias $DH_ALIAS \
            --setdefaultdevhubusername \
            --json
        )
        if [[ $AUTH == *"\"status\": 0"* ]]; then {
            printf "${GREEN} Successfully authorized: $DH_USERNAME as DevHub ${END}\n"
        }; else
            {
                printf "${RED} ERROR: Failed to authorize: $DH_USERNAME ${END}\n"
                printf "${CYAN} MESSAGE: $AUTH ${END}\n"
                exit 1
            }
        fi
    }
fi

# CHECK FOR SO LIMITS WITH DISPLAY
