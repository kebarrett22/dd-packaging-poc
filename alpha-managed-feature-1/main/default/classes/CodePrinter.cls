public with sharing class CodePrinter {
    
    public static String printCodeFancy() {
        return ('~~~ ' + CodeHandler.getCode() + '~~~ ');
    }

}
