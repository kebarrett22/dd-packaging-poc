

# v1.3.3
#### v1.3 - Payment Adaptor
---
* The console now allows:
    * A
    * B
    * C
#### Recent changes
v1.3.1 - New device control permissions for Advisors
v1.3.2 - ACH Transactions added
v1.3.3 - Account Query ordered for pagination

# v1.3.2
#### v1.3 - Payment Adaptor
---
* ACH transactions now supported
#### Recent changes
v1.3.1 - New device control permissions for Advisors 
v1.3.2 - ACH Transactions added

# v1.3.1
#### v1.3 - Payment Adaptor
---
* Augment advisors can now block customer devices
#### Recent changes
v1.3.1 - New device control permissions for Advisors 
