public with sharing class CodeNumerator {

    public static Decimal printCodeNumber() {
        return Decimal.valueOf(CodeHandler.getCode());
    }

}
