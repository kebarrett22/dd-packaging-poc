@namespaceAccessible
public with sharing class CodeHandler {
    
    static String code = '55';
    
    @namespaceAccessible
    public static String getCode() {
        return code;
    }
    
    public static void printWord(String word) {
        System.debug(word);
    }
    
}